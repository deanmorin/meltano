---
description: Learn how to get involved with the Meltano community.
---

# Community

We aim to build Meltano with the support of the community and we want everyone to feel included.

## Guidelines

### Code of Conduct

Everyone interacting in dbt Slack, codebase, issue trackers, and mailing lists are expected to follow the [PyPA Code of Conduct][conduct]. If you are unable to abide by the code of conduct set forth here, we encourage you not to participate in the community.

### Contributor Guide

We welcome contributions to the project! Please review our [Contributor Guide](/docs/contributor-guide.html) on how to contribute.

## Demo Days

We have Demo Days every other week on Friday at 9 AM Pacific (16:00 UTC). Join us on Zoom to share and hear updates from community members and the Meltano team about how they've been using Meltano. We also have the #demo-day Slack channel for planning and discussion.

Our next demo days are:

* 2021-05-07
* 2021-05-21
* 2021-06-04
* 2021-06-18
* 2021-07-02

Use [this link][gcal] to view the full events calendar. View past office hours in our [YouTube Playlist][demodayplaylist].

### Guidelines

We aim to have Demo Day primarily as a way to have community members showcase the work they've been doing, whether it's related to Meltano or not.
As such, we will limit the number of vendors presenting to a maximum of 1 per Demo Day. 

We will prioritize vendors that don't have a strong overlap with the core capabilities of Meltano. 
Ideally, a vendor would be able to share how their product could be compatible with Meltano or with one of the open source plugins detailed in the MeltanoHub.

## Office Hours

We have Office Hours weekly on Wednesday at 9 AM Pacific (16:00 UTC). Join us on Zoom and bring any questions you have to the team. We will share the Zoom link in Slack and via social media prior to the event. There is sometimes a short presentation about something top of mind from the core team, but otherwise this is your time to chat with us and get some help. Use [this link][gcal] to view the events calendar. We also have the #office-hours Slack channel for planning and discussion.

View past office hours in our [YouTube Playlist][officehoursplaylist].

## Slack

Join the <SlackChannelLink>Meltano Slack workspace<OutboundLink /></SlackChannelLink>, which is frequented by the core team and over 850 community members. You can ask any questions you may have in here, or just chat with fellow users.

[conduct]: https://www.pypa.io/en/latest/code-of-conduct/
[officehoursplaylist]: https://www.youtube.com/playlist?list=PLO0YrxtDbWAtuuubcEz7mnCHoGfIf8voT
[demodayplaylist]: https://www.youtube.com/playlist?list=PLO0YrxtDbWAuLRElrtwFI5PwlAEMUi0AD
[gcal]: https://calendar.google.com/calendar/embed?src=c_01cj48ha4h199ctjefi85t9dgc%40group.calendar.google.com

### Channels

We have a number of channels which we use on a regular basis to communicate and share information with the community. 

#### #announcements

This is for posts from the Core Team to the wider community. Here we will share exciting news, upcoming events, new releases, and other relevant information.

##### @Channel usage

We will use `@channel` no more than three times per week and aim to use it a maximum of once per day. 

#### Other Useful Channels

This is not an exhaustive list and we encourage you to join the <SlackChannelLink>Meltano Slack workspace<OutboundLink /></SlackChannelLink> to discover more!

Meltano:

* #contributing - if you want to contribute to Meltano itself
* #hub - all about the [upcoming MeltanoHub](https://gitlab.com/groups/meltano/-/epics/83)
* #plugins-dbt - all things [dbt](https://meltano.com/docs/transforms.html)
* #plugins-general - discussion for any other plugins

Support:

* #best-practices - for learning tips on how others are using Meltano and other data products
* #getting-started - if you need some help [getting up and running with Meltano](https://meltano.com/docs/getting-started.html)
* #troubleshooting - if you get a weird stack trace and need some help

Community Events:

* #demo-day - where we plan and organize [Demo Day](https://meltano.com/docs/community.html#demo-days)
* #office-hours - where we plan and organize weekly [Office Hours](https://meltano.com/docs/community.html#office-hours)

Singer Related:

* #singer-batch-messages - discussion around [Fast Sync](https://gitlab.com/meltano/meltano/-/issues/2364)
* #singer-saas-targets - all things Reverse ETL / Operational Analytics
* #singer-sdk - discussion around the [SDK for Taps and Targets](https://gitlab.com/meltano/singer-sdk)
